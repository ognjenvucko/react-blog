const KeyMirror = require("keymirror");

export const Actions = KeyMirror({
  SHOW_MODAL: null,
  HIDE_MODAL: null,
  EDIT_POST: null,
  SET_CATEGORY: null,
  REQUEST_UPDATE: null,
  SET_MESSAGE: null
});
