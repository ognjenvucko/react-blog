import React, { Suspense, useReducer } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./styles.css";
import { AllPosts } from "./components/AllPosts/AllPosts";
import { CategoryPosts } from "./components/CategoryPosts/CategoryPosts";
import { Header } from "./components/Header/Header";
import Sidebar from "./components/Sidebar/Sidebar";
import { Welcome } from "./components/Welcome/Welcome";
import "./App.scss";
import { Modal } from "./components/Modal/Modal";
import { StateContext, DispatchContext } from "./context";
import { appReducer } from "./reducers";
import { EditForm } from "./components/EditForm/EditForm";

const INITIAL_STATE = {
  showModal: false,
  post: { title: "", text: "" },
  categoryId: null,
  requestCnt: 0,
  appMessage: ""
};

export default function App() {
  const [state, dispatch] = useReducer(appReducer, INITIAL_STATE);
  return (
    <Router>
      <StateContext.Provider value={state}>
        <DispatchContext.Provider value={dispatch}>
          <div className="app-layout">
            <header>
              <Header />
            </header>
            <Welcome />
            <main>
              <Suspense fallback={<div>Loading...</div>}>
                <Switch>
                  <Route path="/" exact>
                    <aside>
                      <Sidebar />
                    </aside>
                    <div className="app-layout__content">
                      <AllPosts />
                    </div>
                  </Route>
                  <Route path="/category/:id">
                    <aside>
                      <Sidebar />
                    </aside>
                    <div className="app-layout__content">
                      <CategoryPosts />
                    </div>
                  </Route>
                </Switch>
              </Suspense>
              <Modal>
                <EditForm />
              </Modal>
            </main>
            {/* <footer>{footer}</footer> */}
          </div>
        </DispatchContext.Provider>
      </StateContext.Provider>
    </Router>
  );
}
