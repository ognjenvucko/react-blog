import React, { useState, useContext } from "react";
import "./EditForm.scss";
import { Button } from "../Button/Button";
import API from "../../lib/api";
import { bemClassNames } from "../../lib/bem";
import { DispatchContext, StateContext } from "../../context";
import { Actions } from "../../actions";

export const EditForm = () => {
  const { post, categoryId } = useContext(StateContext);
  const [title, setTitle] = useState(post.title);
  const [text, setText] = useState(post.text);
  const [category, setCategory] = useState("");
  const [titleAlert, setTitleAlert] = useState(false);
  const [textAlert, setTextAlert] = useState(false);
  const [categoryAlert, setCategoryAlert] = useState(false);
  const dispatch = useContext(DispatchContext);
  const onClose = () => {
    dispatch({
      type: Actions.HIDE_MODAL
    });
    dispatch({
      type: Actions.REQUEST_UPDATE
    });
  };
  const isEdit = post.title && post.text;
  const noCategory = categoryId === null && !isEdit;
  const onSubmit = () => {
    if (!title || !text || (noCategory && !category)) {
      setTitleAlert(!title);
      setTextAlert(!text);
      setCategoryAlert(!category);
      return;
    }
    if (isEdit) {
      API.updatePost(post.id, {
        ...post,
        title,
        text
      })
        .then(onClose)
        .then(() => {
          dispatch({
            type: Actions.SET_MESSAGE,
            payload: "Post edited."
          });
        });
    } else {
      const createPost = id => {
        return API.createNewPost({
          title,
          text,
          categoryId: id
        });
      };
      if (noCategory) {
        API.createCategory({ name: category })
          .then(({ id }) => {
            return createPost(id);
          })
          .then(onClose)
          .then(() => {
            dispatch({
              type: Actions.SET_MESSAGE,
              payload: "Category and post created."
            });
          });
      } else {
        createPost(categoryId)
          .then(onClose)
          .then(() => {
            dispatch({
              type: Actions.SET_MESSAGE,
              payload: "Post created."
            });
          });
      }
    }
  };
  const onTitleChange = e => {
    setTitle(e.target.value);
  };
  const onTextChange = e => {
    setText(e.target.value);
  };
  const onCategoryChange = e => {
    setCategory(e.target.value);
  };
  const titleClass = bemClassNames("title-input", {
    alert: titleAlert
  });
  const textClass = bemClassNames("text-input", {
    alert: textAlert
  });
  const categoryClass = bemClassNames("category-input", {
    alert: categoryAlert
  });
  return (
    <div className="form-wrapper">
      <h2>Add/Edit Blog Post</h2>
      <form>
        {noCategory && (
          <div className="form__category">
            <label htmlFor="categoryName">Category</label>
            <input
              className={categoryClass}
              onChange={onCategoryChange}
              id="categoryName"
              placeholder="Category name"
              type="text"
              value={category}
              maxLength="20"
            />
            <span>*</span>
          </div>
        )}
        <div className="form__title">
          <label htmlFor="postTitle">Title</label>
          <input
            className={titleClass}
            onChange={onTitleChange}
            id="postTitle"
            type="text"
            placeholder="Title of the post"
            value={title}
            maxLength="40"
          />
          <span>*</span>
        </div>
        <div className="form__text">
          <label htmlFor="postText">Text</label>
          <textarea
            maxLength="500"
            className={textClass}
            onChange={onTextChange}
            placeholder="Text of the post"
            id="postText"
            value={text}
          />
          <span>*</span>
        </div>
      </form>
      <div className="form-buttons">
        <Button onClick={onSubmit} text="Post" />
        <Button onClick={onClose} text="Cancel" />
      </div>
    </div>
  );
};
