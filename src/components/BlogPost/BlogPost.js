import React, { useContext } from "react";
import moment from "moment";
import "./BlogPost.scss";
import { Button } from "../Button/Button";
import { DispatchContext } from "../../context";
import { Actions } from "../../actions";

export const BlogPost = ({
  id,
  title,
  text,
  createdAt,
  onDelete,
  categoryId
}) => {
  const formatedDate = moment(createdAt).format("DD.MM.YYYY");
  const formatedTime = moment(createdAt).format("HH:mm");
  const dispatch = useContext(DispatchContext);
  const onEdit = data => {
    dispatch({
      type: Actions.EDIT_POST,
      payload: { ...data }
    });
  };
  return (
    <div key={title} className="blog-post">
      <div className="blog-post__header">
        <div className="blog-post__title-wrapper">
          <div className="blog-post__image" />
          <div className="blog-post__info">
            <div className="blog-post__title">{title}</div>
            <div className="blog-post__timestamp">{`Posted on ${formatedDate} at ${formatedTime}`}</div>
          </div>
        </div>
        <div className="blog-post__actions">
          <Button
            onClick={() => {
              onEdit({
                id,
                title,
                text,
                categoryId
              });
            }}
            text="Edit"
          />
          <Button
            onClick={() => {
              onDelete(id);
            }}
            text="Delete"
          />
        </div>
      </div>
      <div className="blog-post__content">{text}</div>
      <div className="blog-post__images">
        <span />
        <span />
        <span />
      </div>
    </div>
  );
};
