import React, { useContext } from "react";
import "./PostList.scss";
import { BlogPost } from "../BlogPost/BlogPost";
import API from "../../lib/api";
import { DispatchContext } from "../../context";
import { Actions } from "../../actions";

export const PostList = ({ posts }) => {
  const dispatch = useContext(DispatchContext);
  if (!posts) {
    return null;
  }
  const onDelete = id => {
    API.deletePost(id).then(() => {
      dispatch({
        type: Actions.REQUEST_UPDATE
      });
      dispatch({
        type: Actions.SET_MESSAGE,
        payload: "Post deleted."
      });
    });
  };
  const postList = posts.read().resultData;
  if (!postList.length) {
    return <p className="no-posts">No posts yet.</p>;
  }
  return (
    <div className="post-list">
      {postList.map(({ id, title, text, createdAt, categoryId }, idx) => {
        return (
          <BlogPost
            id={id}
            onDelete={onDelete}
            key={idx}
            title={title}
            text={text}
            createdAt={createdAt}
            categoryId={categoryId}
          />
        );
      })}
    </div>
  );
};
