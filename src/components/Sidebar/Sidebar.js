import React, { Suspense, useEffect, useState, useContext } from "react";
import "./Sidebar.scss";
import CategoryList from "../CategoryList/CategoryList";
import API from "../../lib/api";
import { StateContext } from "../../context";

const Sidebar = props => {
  const [categories, setCategories] = useState();
  const { requestCnt } = useContext(StateContext);
  useEffect(() => {
    setCategories(API.fetchCategories());
  }, [requestCnt]);
  return (
    <div className="sidebar">
      <h2>Blog categories</h2>
      <Suspense fallback="Loading">
        <CategoryList categories={categories} />
      </Suspense>
    </div>
  );
};

export default Sidebar;
