import React, { useState, useEffect, useContext } from "react";
import API from "../../lib/api";
import { PostList } from "../PostList/PostList";
import { StateContext } from "../../context";

export const AllPosts = () => {
  const [posts, setPosts] = useState();
  const { requestCnt } = useContext(StateContext);
  useEffect(() => {
    setPosts(API.fetchBlogPosts());
  }, [requestCnt]);
  return <PostList posts={posts} />;
};
