import React, { useState, useEffect, useContext } from "react";
import API from "../../lib/api";
import { PostList } from "../PostList/PostList";
import { useParams } from "react-router-dom";
import { DispatchContext, StateContext } from "../../context";
import { Actions } from "../../actions";

export const CategoryPosts = () => {
  const [posts, setPosts] = useState();
  const dispatch = useContext(DispatchContext);
  const { requestCnt } = useContext(StateContext);
  const { id } = useParams();
  useEffect(() => {
    setPosts(API.fetchBlogPostsForCategory(id));
    dispatch({
      type: Actions.SET_CATEGORY,
      payload: id
    });
    // eslint-disable-next-line
  }, [id, requestCnt]);
  return <PostList posts={posts} />;
};
