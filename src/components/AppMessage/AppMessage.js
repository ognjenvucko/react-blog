import React, { useContext } from "react";
import "./AppMessage.scss";
import { StateContext, DispatchContext } from "../../context";
import { Actions } from "../../actions";

export const AppMessage = () => {
  const { appMessage } = useContext(StateContext);
  const dispatch = useContext(DispatchContext);
  if (!appMessage) {
    return null;
  }
  const onHide = () => {
    dispatch({
      type: Actions.SET_MESSAGE,
      payload: ""
    });
  };
  return (
    <div className="app-message">
      <span onClick={onHide} className="app-message__close">
        x
      </span>
      {appMessage}
    </div>
  );
};
