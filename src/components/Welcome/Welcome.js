import React, { useContext } from "react";
import "./Welcome.scss";
import { AppMessage } from "../AppMessage/AppMessage";
import { Button } from "../Button/Button";
import { DispatchContext } from "../../context";
import { Actions } from "../../actions";

export const Welcome = () => {
  const dispatch = useContext(DispatchContext);
  return (
    <div className="app-welcome">
      <aside />
      <div className="app-welcome__content">
        <h2>Welcome to My Blog</h2>
        <AppMessage />
        <div className="app-welcome__add-post-btn">
          <Button
            text="Add post"
            onClick={() => {
              dispatch({
                type: Actions.SHOW_MODAL
              });
            }}
          />
        </div>
      </div>
    </div>
  );
};
