import React from "react";
import { useParams } from "react-router";
import { NavLink } from "react-router-dom";
import "./CategoryList.scss";

const CategoryList = ({ categories }) => {
  const { id: paramId } = useParams();
  if (!categories) {
    return null;
  }
  const categoriesList = categories.read().resultData;
  if (!categoriesList.length) {
    return <p className="no-categories">No categories.</p>;
  }
  return (
    <ul>
      {categoriesList.map(({ id, name }, idx) => {
        const isSelected = id === +paramId;
        return (
          <li key={`c${idx}-${name}`}>
            {isSelected ? (
              <span>{name}</span>
            ) : (
              <NavLink to={`/category/${id}`}>{name}</NavLink>
            )}
          </li>
        );
      })}
    </ul>
  );
};

export default CategoryList;
