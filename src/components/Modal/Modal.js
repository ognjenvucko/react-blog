import React, { useContext } from "react";
import "./Modal.scss";
import { StateContext, DispatchContext } from "../../context";
import { Actions } from "../../actions";

export const Modal = ({ children }) => {
  const { showModal } = useContext(StateContext);
  const dispatch = useContext(DispatchContext);
  if (!showModal) {
    return null;
  }
  const closeModal = e => {
    dispatch({
      type: Actions.HIDE_MODAL
    });
    e.preventDefault();
    e.stopPropagation();
  };
  return (
    <React.Fragment>
      <div onClick={closeModal} className="modal-overlay" />
      <div className="modal">
        {children}
        <span onClick={closeModal} className="modal__close">
          x
        </span>
      </div>
    </React.Fragment>
  );
};
