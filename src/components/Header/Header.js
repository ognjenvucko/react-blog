import React, { useState, useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";
import "./Header.scss";
import { bemClassNames } from "../../lib/bem";
import { NavLink } from "react-router-dom";
import { DispatchContext } from "../../context";
import { Actions } from "../../actions";

export const Header = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const dispatch = useContext(DispatchContext);
  const navClass = bemClassNames("navigation", {
    open: isMenuOpen
  });
  return (
    <div className="app-header">
      <div className="app-logo">
        <NavLink
          onClick={() => {
            dispatch({
              type: Actions.SET_CATEGORY,
              payload: null
            });
          }}
          to="/"
        >
          My Blog
        </NavLink>
      </div>
      <div className="app-nav">
        <input type="text" placeholder="Search" />
        <nav className={navClass}>
          <a href="/#">My Profile</a>
          <a href="/#">Logout</a>
          <FontAwesomeIcon
            className="nav-burger"
            onClick={() => {
              setIsMenuOpen(!isMenuOpen);
            }}
            icon={isMenuOpen ? faTimes : faBars}
            size="lg"
            color="#fff"
          />
        </nav>
      </div>
    </div>
  );
};
