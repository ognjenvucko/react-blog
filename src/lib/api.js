import qs from "query-string";
import { API_URL } from "./config";
import { wrapPromise } from "./suspense";

const fetchBlogPosts = async () => {
  return await fetch(`${API_URL}/BlogPosts`).then(response => {
    return response.json();
  });
};

const fetchBlogPostsForCategory = async categoryId => {
  const query = qs.stringify({ categoryId });
  return await fetch(`${API_URL}/BlogPosts/GetPostByCategory?${query}`).then(
    response => {
      return response.json();
    }
  );
};

const fetchCategories = async () => {
  return await fetch(`${API_URL}/Category`).then(response => {
    return response.json();
  });
};

const createCategory = async data => {
  const response = fetch(`${API_URL}/Category`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  });
  return (await response).json();
};

const createNewPost = async data => {
  const response = fetch(`${API_URL}/BlogPosts`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  });
  return (await response).json();
};

const updatePost = async (id, data) => {
  const response = fetch(`${API_URL}/BlogPosts/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  });
  return response;
};

const deletePost = async id => {
  const response = fetch(`${API_URL}/BlogPosts/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json"
    }
  });
  return response;
};

const API = {
  fetchBlogPosts: () => {
    return wrapPromise(fetchBlogPosts());
  },
  fetchCategories: () => {
    return wrapPromise(fetchCategories());
  },
  fetchBlogPostsForCategory: categoryId => {
    return wrapPromise(fetchBlogPostsForCategory(categoryId));
  },
  createNewPost,
  createCategory,
  updatePost,
  deletePost
};

export default API;
