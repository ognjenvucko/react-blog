import keyMirror from "keymirror";

const STATUS = keyMirror({
  PENDING: null,
  SUCCESS: null,
  ERROR: null
});

export const wrapPromise = promise => {
  let status = STATUS.PENDING;
  let result = null;
  const suspender = promise.then(
    data => {
      status = STATUS.SUCCESS;
      result = data;
    },
    err => {
      status = STATUS.ERROR;
      result = err;
    }
  );
  return {
    read: () => {
      if (status === STATUS.PENDING) {
        throw suspender;
      }
      if (status === STATUS.SUCCESS) {
        return result;
      }
      // if error
      throw result;
    }
  };
};
