import { Actions } from "../actions";

export const appReducer = (state, action) => {
  switch (action.type) {
    case Actions.SHOW_MODAL: {
      return {
        ...state,
        showModal: true
      };
    }
    case Actions.HIDE_MODAL: {
      return {
        ...state,
        showModal: false,
        post: { title: "", text: "" }
      };
    }
    case Actions.EDIT_POST: {
      return {
        ...state,
        post: {
          ...state.post,
          ...action.payload
        },
        showModal: true
      };
    }
    case Actions.SET_CATEGORY: {
      return {
        ...state,
        categoryId: action.payload
      };
    }
    case Actions.REQUEST_UPDATE: {
      return {
        ...state,
        requestCnt: state.requestCnt + 1
      };
    }
    case Actions.SET_MESSAGE: {
      return {
        ...state,
        appMessage: action.payload
      };
    }
    default: {
      return {
        ...state
      };
    }
  }
};
